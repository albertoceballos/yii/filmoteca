<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Roles;


class RolesController extends Controller{
    
    public function actionIndex(){
        
        $consulta= Roles::find()->all();
        return $this->render('index',['datos'=>$consulta]);
    }
    
    public function actionAjax(){
        $id= \yii::$app->request->post("id");
        $consulta= \Yii::$app->db->createCommand("SELECT * FROM artistas JOIN participan p USING (id_artista) JOIN roles r USING(id_rol)
  WHERE r.id_rol=$id GROUP BY p.id_artista")->queryAll();
        
       if(sizeof($consulta)==0){
           echo '<option value="0">No hay artistas con ese rol</option>';
        }else{
            foreach($consulta as $reg){
                echo '<option value="'.$reg['id_artista'].'">'.$reg["artista"].'</option>'; 
            }
        }
        
    }
    
    public function actionBuscarpeliculas($artista){
        
        $consulta= Roles::find()->all();
        $busqueda= \app\models\Producciones::findBySql("SELECT * FROM producciones 
                JOIN participan USING (id_produccion)
                JOIN artistas USING (id_artista)
                WHERE artistas.id_artista=$artista")->all();
        
        return $this->render('index',['datos'=>$consulta,'busqueda'=>$busqueda]);
        
    }
    
}


