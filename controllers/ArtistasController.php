<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Artistas;
use app\models\Producciones;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class ArtistasController extends Controller{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [''],
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionIndex(){
        //consulta para extraer a los artistas que son actores
        $actores= Artistas::findBySql('SELECT  * FROM artistas 
        JOIN participan USING (id_artista)
        JOIN roles USING (id_rol)
        WHERE rol ="actor/actriz" GROUP BY artistas.id_artista ORDER BY artista')->all();
        
        //consulta para extraer a los artistas que son directores
        $directores= Artistas::findBySql('SELECT  * FROM artistas 
        JOIN participan USING (id_artista)
        JOIN roles USING (id_rol)
        WHERE rol ="director" GROUP BY artistas.id_artista ORDER BY artista')->all();
        
        return $this->render('index',['actores'=>$actores,'directores'=>$directores]);
    }
    
    public function actionMuestraartistas($actores='',$directores=''){
        //consulta para extraer a los artistas que son actores
        $listaactores= Artistas::findBySql('SELECT  * FROM artistas 
        JOIN participan USING (id_artista)
        JOIN roles USING (id_rol)
        WHERE rol ="actor/actriz" GROUP BY artistas.id_artista ORDER BY artista')->all();
        
        //consulta para extraer a los artistas que son directores
        $listadirectores= Artistas::findBySql('SELECT  * FROM artistas 
        JOIN participan USING (id_artista)
        JOIN roles USING (id_rol)
        WHERE rol ="director" GROUP BY artistas.id_artista ORDER BY artista')->all();
        
        //Para la búsqueda del artista que se haya enviado, actor o director
        if($actores!=''){
           $artista=$actores;
        }else{
            if($directores!=''){
                $artista=$directores;
            }   
        }
         $consulta= Producciones::findBySql("SELECT * FROM producciones 
                JOIN participan USING (id_produccion)
                JOIN artistas USING (id_artista)
                WHERE artistas.id_artista=$artista")->all();
        
        return $this->render('index',['actores'=>$listaactores,'directores'=>$listadirectores,'consulta'=>$consulta]);
    }
    
    
    public function actionBuscaactordirector($actor,$director){
        
        //consulta para extraer a los artistas que son actores
        $listaactores= Artistas::findBySql('SELECT  * FROM artistas 
        JOIN participan USING (id_artista)
        JOIN roles USING (id_rol)
        WHERE rol ="actor/actriz" GROUP BY artistas.id_artista ORDER BY artista')->all();
        
        //consulta para extraer a los artistas que son directores
        $listadirectores= Artistas::findBySql('SELECT  * FROM artistas 
        JOIN participan USING (id_artista)
        JOIN roles USING (id_rol)
        WHERE rol ="director" GROUP BY artistas.id_artista ORDER BY artista')->all();
        
        //Consulta para buscar un actor y un director al ,ismo tiempo
        $consulta= Producciones::findBySql("SELECT producciones.* FROM producciones
            JOIN participan p USING (id_produccion) JOIN participan p1 USING (id_produccion)
            WHERE p.id_artista=$actor AND p1.id_artista=$director")->all();
        
        return $this->render('index',['actores'=>$listaactores,'directores'=>$listadirectores,'consulta'=>$consulta]);
       
    }
}




