<?php

namespace app\controllers;

use app\models\Generos;
use yii\web\Controller;
use app\models\Producciones;


class GenerosController extends Controller{
    
    public function actionIndex(){
        
     $generos= Generos::find()->asArray()->all();   
      return $this->render("index",["generos"=>$generos]);
    }


    public function actionFiltragenero($genero){
        $generos= Generos::find()->asArray()->all();   
        $consulta= Producciones::findBySql("SELECT * FROM producciones JOIN proyecta USING (id_produccion) JOIN generos USING (id_genero) WHERE generos.id_genero=$genero")
                ->all();
        return $this->render("index",["generos"=>$generos,"consulta"=>$consulta,'genero'=>$genero]);
    }
    
}

