<?php

use yii\helpers\Html;
use app\models\Producciones;
use app\models\Artistas;
//var_dump($datos);

?>
<?php 
 
  
    echo Html::beginForm(['generos/filtragenero'], 'get');
    echo "Géneros: ";
    echo Html::dropDownList('genero', null, \yii\helpers\ArrayHelper::map($generos,'id_genero','genero'),
            ['prompt'=>'Selecciona un género',
             'id'=>'select1',
             'onchange'=>'cambiaSelect()',    
            ]);
    echo " ".Html::submitButton('Buscar',['class'=>'submit btn btn-primary','id'=>'enviar','disabled'=>'true']);
    echo Html::endform();
 ?>   
    

<?php  
//si se está buscando un género en concreto, muestra los resultados
if(isset($consulta)){      

  if(sizeof($consulta)==0){
      echo "No hay películas con ese género";
  }
    
foreach($consulta as $registro){
   
?>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
        <?= Html::img('@web/fotos/'.$registro->cartel,['alt'=>$registro->titulo,'width'=>'250px'])?>
      <div class="caption">
        <h3> <?= $registro->titulo ?></h3>
        <p><?= Producciones::instance()->getAttributeLabel("id_produccion").": ".$registro->id_produccion ?></p>
        <p><?= Producciones::instance()->getAttributeLabel("estreno").": ".$registro->estreno ?> </p>
        <p><?= Producciones::instance()->getAttributeLabel("puntuacion").": ".$registro->puntuacion ?> </p>
        <p><?=Producciones::instance()->getAttributeLabel("nacionalidad").": ".$registro->nacionalidad ?> </p>
        <p><?php 
             $actores=$registro->getParticipans()->all();
            foreach ($actores as $actor){    
                $nombres=Artistas::find()->where(["id_artista"=>$actor->id_artista])->asArray()->all();
                
                foreach($nombres as $nombre){
                    echo $nombre["artista"]."<br>";
                }
                
                $roles= \app\models\Roles::find()->where(["id_rol"=>$actor->id_rol])->asArray()->all();
                foreach($roles as $rol){
                    echo $rol["rol"]."<br>";
                }
                
            }
        ?> 
        </p>
            <p><a class="btn btn-primary"role="button" data-toggle="collapse" href="#collapse<?= $registro->id_produccion ?>" aria-expanded="false" aria-controls="collapse1">
             Mostrar Géneros
            </a>
   
            <div class="collapse" id="collapse<?= $registro->id_produccion ?>">
                <div class="well">
                    <?php
                        $generos=$registro->getGeneros()->all();
                        foreach($generos as $genero){
                           echo $genero->genero."<br>";
                        }
                    ?>
                </div>
            </div>
      </div>
    </div>
  </div>
<?php
    }
}
?>
<script type="text/javascript">
    function cambiaSelect(){
        var select1=document.querySelector("#select1");
        var valor=select1.options[select1.selectedIndex].value;
        console.log(valor);
        if(valor!==""){
         document.querySelector("#enviar").disabled=false;
        }else{
          document.querySelector("#enviar").disabled=true;
        }
    }
</script>
    
   

