<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\components\PeliculasWidget;

?>
<div class="container">
    <div class="row">
        <div class="col-xs-4">
         Selecciona un rol para ver los artistas del mismo:
        </div>
        <div class="col-xs-4">
          Artistas:
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <?= Html::dropDownList("roles", null, ArrayHelper::map($datos, 'id_rol', 'rol'),
                    ['onchange'=>'cambiaRol(this)',
                     'id'=>'selRoles',
                     'prompt'=>'Elige un rol']);?>    
        </div>
        <div class="col-xs-6">
            <?= Html::beginForm('buscarpeliculas','get') ?>
            <select name="artista" id="selectArtistas" disabled="true">
                <option value="">Ningun rol seleccionado</option>
                
            </select>
            <?= Html::submitButton("Buscar películas del artista",['class'=>'btn btn-success', 'disabled'=>'true','id'=>'submitBuscar']) ?>
            <?= Html::endForm(); ?>
        </div>
    </div>
    <div class="row">
        <?php 
        if(isset($busqueda)){
            
           echo PeliculasWidget::widget(['busqueda'=>$busqueda]);
                   
        }
        ?>
    </div>
</div>
<script type="text/javascript">
    
    function cambiaRol(e){
       idSeleccionado=e.options[e.selectedIndex].value;
       if(idSeleccionado!==""){
       $("#selectArtistas").prop('disabled',false);
        $.ajax({
            url:'ajax',
            type:'post',
            data:{'id':idSeleccionado},
            success:function(resultado){ 
                $('#selectArtistas').html(resultado);
                 idArtista=$("#selectArtistas").val();
                 console.log(idArtista);
                 if(idArtista!=0){
                     $("#submitBuscar").prop('disabled',false);
                 }else{
                     $("#submitBuscar").prop('disabled',true);
                 }
            }
        });
       }
      
    }
    
</script>