<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Producciones */

$this->title = 'Update Producciones: ' . $model->id_produccion;
$this->params['breadcrumbs'][] = ['label' => 'Producciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_produccion, 'url' => ['view', 'id' => $model->id_produccion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="producciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
