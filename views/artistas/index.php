<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\components\PeliculasWidget;

?>
<div class="container text-center">

    <div class="div1 text-justify" style="display: inline-block">
<?= Html::beginForm('muestraartistas', 'get'); ?>
<?= Html::dropDownList('actores', null, ArrayHelper::map($actores, 'id_artista','artista'),
        ['prompt'=>'Elige un actor',
         'id'=>'selectActores',
         'onchange'=>'cambiaSeleccion(this,"#enviarActores")',
        ]) ?>
        
<?= " ".Html::submitButton('Buscar por actor', ['class'=>'btn btn-primary','disabled'=>'true','id'=>'enviarActores'])?>
<?= Html::endForm(); ?>

<br>
<?= Html::beginForm('muestraartistas','get') ?>
<?= Html::dropDownList('directores',null, ArrayHelper::map($directores,'id_artista', 'artista'),
        ['prompt'=>'Elige un director',
         'id'=>'selectDirectores',
         'onchange'=>'cambiaSeleccion(this,"#enviarDirectores")',
        ]) ?>
<?= " ".Html::submitButton('Buscar por director',['class'=>'btn btn-primary','disabled'=>'true','id'=>'enviarDirectores'])?>
<?= Html::endForm(); ?>
<br>
<?= Html::beginForm('buscaactordirector','get',['id'=>'form3'])?>

<?= Html::hiddenInput('actor','' ,['id'=>'hiddenActor']) ?>
<?= Html::hiddenInput('director','' ,['id'=>'hiddenDirector']) ?>
<?= Html::Button("Buscar por Actor y Director",['class'=>'btn btn-primary',
                        'disabled'=>'true',
                        'id'=>'enviarAmbos',
                        'onclick'=>'enviar2()']) ?>
<?= Html::endForm()?>

    </div>
</div>

<?php
if(isset($consulta)){
    
    if(sizeof($consulta)==0){
        echo "No hay películas coincidentes con actor y director simultáneamente";
    }
    echo PeliculasWidget::widget(['busqueda'=>$consulta]);

}
?>
<script type="text/javascript">
    function cambiaSeleccion(e,boton){
        valor=e.options[e.selectedIndex].value;
        if(valor!==""){
            document.querySelector(boton).disabled=false;
        }else{
            document.querySelector(boton).disabled=true;
        }
        
        arraySelects=document.querySelectorAll("select");
        arrayValores=[];
        for(i=0;i<arraySelects.length;i++){
            arrayValores[i]=arraySelects[i].options[arraySelects[i].selectedIndex].value;
        }
        
        var vacio=0;
        for(i=0;i<arrayValores.length;i++){
            if(arrayValores[i]==""){
               vacio=1;
            }
            //console.log(arrayValores[i]);
        }
        console.log("vacio: "+vacio);
        if(vacio==1){
            document.querySelector("#enviarAmbos").disabled=true;
        }else{
            document.querySelector("#enviarAmbos").disabled=false;
        }
    }
    
    function enviar2(){

        valActor=document.querySelector("#selectActores").options[document.querySelector("#selectActores").selectedIndex].value;
        valDirector=document.querySelector("#selectDirectores").options[document.querySelector("#selectDirectores").selectedIndex].value;
        document.querySelector("#hiddenActor").value=valActor;
        document.querySelector("#hiddenDirector").value=valDirector;
        document.querySelector("#form3").submit();
    }
    

</script>

