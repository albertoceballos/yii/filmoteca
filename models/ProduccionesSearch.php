<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Producciones;

/**
 * ProduccionesSearch represents the model behind the search form of `app\models\Producciones`.
 */
class ProduccionesSearch extends Producciones
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_produccion', 'estreno', 'vos', 'vista'], 'integer'],
            [['titulo', 'nacionalidad'], 'safe'],
            [['puntuacion'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Producciones::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_produccion' => $this->id_produccion,
            'estreno' => $this->estreno,
            'puntuacion' => $this->puntuacion,
            'vos' => $this->vos,
            'vista' => $this->vista,
        ]);

        $query->andFilterWhere(['like', 'titulo', $this->titulo])
            ->andFilterWhere(['like', 'nacionalidad', $this->nacionalidad]);

        return $dataProvider;
    }
}
