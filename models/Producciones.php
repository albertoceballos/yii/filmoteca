<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producciones".
 *
 * @property int $id_produccion
 * @property string $titulo
 * @property int $estreno
 * @property double $puntuacion
 * @property int $vos
 * @property string $nacionalidad
 * @property int $vista
 *
 * @property Participan[] $participans
 * @property Pertenece[] $perteneces
 * @property Franquicias[] $franquicias
 * @property Proyecta[] $proyectas
 * @property Generos[] $generos
 * @property Temporadas[] $temporadas
 */
class Producciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['estreno', 'vos', 'vista'], 'integer'],
            [['puntuacion'], 'number'],
            [['titulo', 'nacionalidad'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_produccion' => 'Id Produccion',
            'titulo' => 'Titulo',
            'estreno' => 'Estreno',
            'puntuacion' => 'Puntuacion',
            'vos' => 'Versión Original',
            'nacionalidad' => 'Nacionalidad',
            'vista' => 'Vista',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipans()
    {
        return $this->hasMany(Participan::className(), ['id_produccion' => 'id_produccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerteneces()
    {
        return $this->hasMany(Pertenece::className(), ['id_produccion' => 'id_produccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFranquicias()
    {
        return $this->hasMany(Franquicias::className(), ['id_franquicia' => 'id_franquicia'])->viaTable('pertenece', ['id_produccion' => 'id_produccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectas()
    {
        return $this->hasMany(Proyecta::className(), ['id_produccion' => 'id_produccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneros()
    {
        return $this->hasMany(Generos::className(), ['id_genero' => 'id_genero'])->viaTable('proyecta', ['id_produccion' => 'id_produccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporadas()
    {
        return $this->hasMany(Temporadas::className(), ['id_produccion' => 'id_produccion']);
    }
}
