<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "generos".
 *
 * @property int $id_genero
 * @property string $genero
 *
 * @property Proyecta[] $proyectas
 * @property Producciones[] $produccions
 */
class Generos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'generos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['genero'], 'required'],
            [['genero'], 'string', 'max' => 255],
            [['genero'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_genero' => 'Id Genero',
            'genero' => 'Genero',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectas()
    {
        return $this->hasMany(Proyecta::className(), ['id_genero' => 'id_genero']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduccions()
    {
        return $this->hasMany(Producciones::className(), ['id_produccion' => 'id_produccion'])->viaTable('proyecta', ['id_genero' => 'id_genero']);
    }
}
