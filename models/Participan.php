<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "participan".
 *
 * @property int $id_produccion
 * @property int $id_artista
 * @property int $id_rol
 *
 * @property Producciones $produccion
 * @property Artistas $artista
 * @property Roles $rol
 */
class Participan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'participan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_produccion', 'id_artista', 'id_rol'], 'required'],
            [['id_produccion', 'id_artista', 'id_rol'], 'integer'],
            [['id_produccion', 'id_artista', 'id_rol'], 'unique', 'targetAttribute' => ['id_produccion', 'id_artista', 'id_rol']],
            [['id_produccion'], 'exist', 'skipOnError' => true, 'targetClass' => Producciones::className(), 'targetAttribute' => ['id_produccion' => 'id_produccion']],
            [['id_artista'], 'exist', 'skipOnError' => true, 'targetClass' => Artistas::className(), 'targetAttribute' => ['id_artista' => 'id_artista']],
            [['id_rol'], 'exist', 'skipOnError' => true, 'targetClass' => Roles::className(), 'targetAttribute' => ['id_rol' => 'id_rol']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_produccion' => 'Id Produccion',
            'id_artista' => 'Id Artista',
            'id_rol' => 'Id Rol',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduccion()
    {
        return $this->hasOne(Producciones::className(), ['id_produccion' => 'id_produccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtista()
    {
        return $this->hasOne(Artistas::className(), ['id_artista' => 'id_artista']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRol()
    {
        return $this->hasOne(Roles::className(), ['id_rol' => 'id_rol']);
    }
}
