<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artistas".
 *
 * @property int $id_artista
 * @property string $artista
 * @property int $nacimiento
 *
 * @property Participan[] $participans
 */
class Artistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'artistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nacimiento'], 'integer'],
            [['artista'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_artista' => 'Id Artista',
            'artista' => 'Artista',
            'nacimiento' => 'Nacimiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipans()
    {
        return $this->hasMany(Participan::className(), ['id_artista' => 'id_artista']);
    }
}
