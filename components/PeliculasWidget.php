<?php

namespace app\components;

use yii\base\Widget;


class PeliculasWidget extends Widget{
    
    public $busqueda;
    
    public function init() {
        parent::init();
        
    }
    
    public function run(){
        return $this->render('busquedaPeliculas',['busqueda'=> $this->busqueda]);
           
    }
 }


